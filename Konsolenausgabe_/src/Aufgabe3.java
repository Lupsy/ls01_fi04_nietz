
public class Aufgabe3 {

	public static void main(String[] args) {

		System.out.println("Aufgabe 3");
		System.out.println("");
		System.out.println("");
		
		//Variablen Fahrenheit
		int f0 = -20;
		int f1 = -10;
		int f2 = 0;
		int f3 = 20;
		int f4 = 30;
		
		//Variablen Celsius
		double c0 = -28.8889;
		double c1 = -23.3333;
		double c2 = -17.7778;
		double c3 = -6.6667;
		double c4 = -1.1111;
		
		//Kopfzeile
		System.out.printf("%-12s", "Fahrenheit");
		System.out.print("|");
		System.out.printf("%10s\n", "Celsius");
		System.out.printf("%.23s\n", "----------------------------------------------------------");
		
		//Tabellenzeile 1
		System.out.printf("%-12d", f0);
		System.out.print("|");
		System.out.printf("%10.2f\n", c0);
		//Tabellenzeile 2
		System.out.printf("%-12d", f1);
		System.out.print("|");
		System.out.printf("%10.2f\n", c1);		
		//Tabellenzeile 3
		System.out.printf("%+-12d", f2);
		System.out.print("|");
		System.out.printf("%10.2f\n", c2);		
		//Tabellenzeile 4
		System.out.printf("%+-12d", f3);
		System.out.print("|");
		System.out.printf("%10.2f\n", c3);		
		//Tabellenzeile 4
		System.out.printf("%+-12d", f4);
		System.out.print("|");
		System.out.printf("%10.2f\n", c4);
	}

}
