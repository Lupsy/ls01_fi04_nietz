﻿import java.util.Scanner;

class Fahrkartenautomat {
	public static Scanner tastatur = new Scanner(System.in);

	public static void main(String[] args) {
		while(true) {
	       double eingezahlterGesamtbetrag;
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
	       
	       // Geldeinwurf
	       // -----------
	       eingezahlterGesamtbetrag = geldeinwurf(zuZahlenderBetrag);
	
	       // Fahrscheinausgabe
	       // -----------------
	       fahrscheinausgabe();
	       
	       // Rückgeldberechnung und -Ausgabe
	       // -------------------------------
	       rueckgeldberechnung(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	       
		}
    }

	public static double fahrkartenbestellungErfassen() {
		double gesamtBetrag = 0;
		int anzahlKarten = 1;
		int ticket = 0;

		String[] fahrkartenBezeichnung = {
				"Einzelfahrschein Berlin AB",
				"Einzelfahrschein Berlin BC",
				"Einzelfahrschein Berlin ABC",
				"Kurzstrecke",
				"Tageskarte Berlin AB",
				"Tageskarte Berlin BC",
				"Tageskarte Berlin ABC",
				"Kleingruppen-Tageskarte Berlin AB",
				"Kleingruppen-Tageskarte Berlin BC",
				"Kleingruppen-Tageskarte Berlin ABC",
		};
		
		double[] fahrkartenPreis = {
				2.90,
				3.30,
				3.60,
				1.90,
				8.60,
				9.00,
				9.60,
				23.50,
				24.30,
				24.90,
		};
		
		int arrayLength = fahrkartenBezeichnung.length;
		
		/* Q: Welche Vorteile hat man durch diesen Schritt?
		 * A: Hinzufügen von Fahrkarten ist einfach durch einfügen von neuen Einträgen in beiden Arrays zu verwirklichen.
		 */
		
		/* Länge des Arrays als maximale länge der Schleife, da beide Arrays gleich groß sein müssen, 
		 * ist es egal welches array man für die länge nimmt.
		*/
		
		do{
			System.out.printf("%-16s%-40s%-16s%n","Auswahlnummer", "Bezeichnung", "Preis in Euro");
			for(int x = 0; x < arrayLength; x++) {
				System.out.printf("%-16d%-40s%-4.2f€%n", (x+1), fahrkartenBezeichnung[x], fahrkartenPreis[x]);
			}
			System.out.printf("%n%-16d%-40s%n", (arrayLength+1), "Zum Bezahlen");
			
			double zuZahlenderBetrag = 0;
			System.out.print("\nWelches Ticket?: ");
			ticket = tastatur.nextInt();
			
			if(ticket != (arrayLength+1)) {
				zuZahlenderBetrag = fahrkartenPreis[ticket-1];
			
				boolean ticketPruefung = true;
				
				while (ticketPruefung) {
					System.out.print("\nAnzahl der Tickets (1 - 10): ");
					anzahlKarten = tastatur.nextInt();
					if (anzahlKarten > 0 && anzahlKarten <= 10) {
						ticketPruefung = false;
					}
				}
				gesamtBetrag += zuZahlenderBetrag * anzahlKarten;
			}
		}while(ticket != (arrayLength+1));
	
		return gesamtBetrag;
		
		/* Q: Vergleichen Sie die neue Implementierung mit der alten und erläutern Sie die Vor- und Nachteile der jeweiligen Implementierung.
		 * A: Die Array Implementierung vereinfacht das hinzufügen von neuen Tickets ohne den Rest des Programmes weiter zu ändern.
		 * 	  Die alte Implementierung wäre in diesem Falle umständlich zu verändern, da man an mehreren Stellen neue Einträge erstellen muss.
		 *    Vorteile der alten Version sind mir nicht bekannt. Nachteile der Arrays sind das kompliziertere Angebote nicht einfach realisierbar sind,
		 *    und ArrayOutOfBoundsException are a thing.
		 */
	}
	
	//FahrkarteBezahlen
	public static double geldeinwurf(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.0;
		double eingeworfeneMünze = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("%s%.2f%s%n", "Noch zu zahlen: ", (zuZahlenderBetrag - eingezahlterGesamtbetrag), " €");
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag;
	}

	public static void fahrscheinausgabe() {
		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void rueckgeldberechnung(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rückgabebetrag = Math.round((eingezahlterGesamtbetrag - zuZahlenderBetrag) * 100.0) / 100.0;
		if (rückgabebetrag > 0.0) {
			System.out.printf("%s%.2f%s%n", "Der Rückgabebetrag in Höhe von ", rückgabebetrag, " EURO");
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rückgabebetrag >= 2.0)  // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rückgabebetrag -= 2.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rückgabebetrag -= 1.0;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "Cent");
				rückgabebetrag -= 0.5;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				muenzeAusgeben(20, "CENT");
				rückgabebetrag -= 0.2;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rückgabebetrag -= 0.1;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
			while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rückgabebetrag -= 0.05;
				rückgabebetrag = Math.round(rückgabebetrag * 100.0) / 100.0;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.");
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}

}