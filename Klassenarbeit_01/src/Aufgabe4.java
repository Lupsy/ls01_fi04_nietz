
public class Aufgabe4 {
	
	public static void main(String[] args) {
		int zahl = 12;
		System.out.print(romanNumerals(zahl));
	}
	
	//hier soll Ihr Quelltext hin
	public static String romanNumerals(int zahl) {
		String roman4life = "cool";
		
		if(zahl == 1) {
			roman4life = "I";
		}
		else if (zahl == 5) {
			roman4life = "V";
		}
		else if (zahl == 10) {
			roman4life = "X";
		}
		else if (zahl == 50) {
			roman4life = "L";
		}
		else if (zahl == 100) {
			roman4life = "C";
		}
		else if (zahl == 500) {
			roman4life = "D";
		}
		else if (zahl == 1000) {
			roman4life = "M";
		}
		else {
			roman4life = "?";
		}
		
		return roman4life;
	}
	
}
