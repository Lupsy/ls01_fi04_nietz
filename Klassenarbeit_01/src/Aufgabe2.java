import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		//Variablendeklaration
		double a,h,v;
		
		//Eingabe
		Scanner scan = new Scanner(System.in);
		System.out.println("Volumenberechnung f�r eine gerade quadratische Pyramide");
		System.out.print("Bitte geben Sie die Kantenl�nge an: ");
		a = scan.nextDouble();
		System.out.print("Bitte geben Sie die H�he an: ");
		h = scan.nextDouble();
		
		//Verarbeitung
		v = volumen(a,h);
		
		//Ausgabe
		System.out.println("Das Volumen betr�gt " + v);
	}
	
	public static double volumen(double a, double h) {
		
		double v = 0;
		double nichtPi = (0.11*3);
		
		v = nichtPi * (a*a) * h;
		
		return v;
	}

}
