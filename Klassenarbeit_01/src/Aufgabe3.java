/** Fehlersuche
 * @version 3.0 vom 01.12.2020
 * @author Tenbusch
 */  
import java.util.Scanner;

public class Aufgabe3 {
    
	public static void main(String[] args) {
    	
		umfangsberechnung();
		
	}
    
    public static String umfangsberechnung() {  //die Zeile ist korrekt, Finger weg! 			"Okay :D"
    	
    	//Variablendeklaration
    	Scanner eingabe = new Scanner(System.in);
    	final double PI = 3.141;
    	double durchmesser, umfang;
    	String ueberschrift = "Umfangsberechnung eines eckigen Kreises";
        
    	//Eingabe
    	System.out.println(ueberschrift);
    	System.out.println("Bitte geben Sie den Durchmesser ein: ");
    	durchmesser = eingabe.nextDouble();
    	
    	//Verarbeitung
    	umfang = 2 * PI * durchmesser;
        
    	//Ausgabe
    	System.out.println("Der Umfang betr�gt " + umfang);
    	
    	//Automatisierte Auswertung  ------ab hier ist alles korrekt, nichts ver�ndern------
    	return "A2: " + ueberschrift + ";" + PI + "; Durchmesser: " + durchmesser + "; Umfang: " + umfang;
    }
}